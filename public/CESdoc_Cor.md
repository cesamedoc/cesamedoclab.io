# Création de ticket avec un profil Correspondant
1. [Principe](#iprincipe)
2. [Ouvrir un ticket](#iiouvrirunticket)
3. [Interagir avec un ticket](#iiiinteragiravecunticket)
4. [Inventaire](#iiiiinventaire)

## I. Principe
Pour accéder à l'outil CESAME, Vous devez disposer d'un compte ENT (MonLycée.net) actif. Le profil correspondant est à demander auprès de votre responsable d'établissement pour autorisation. Après validation vous devez vous rapprocher du service CESAME afin de vous ajouter le droit souhaité.  
<sub>[Haut de page](#crationdeticketavecunprofilcorrespondant "Remonter")</sub>

## II. Ouvrir un ticket
Sélectionnez ``Créer un ticket`` dans la liste déroulante de l'onglet ![](./img/onglet_Assistance.png). Une fois l'interface de création ouverte, différents champs obligatoire sont à compléter pour valider votre ticket.

  ![](./img/Creation_Ticket_COR.png)

1. Affiche la date et heure d’ouverture du ticket

2. Réservé à CESAME. Information mise à jour automatique par rapport à l'urgence du ticket (voir point N°8).

3. Indiquer le type de requête parmi les deux possibilités.
  - Demande (Renouvellement matériel, installation,...)
  - Incident (Dysfonctionnement, panne, erreur, etc...)

4. Sélectionner la catégorie du ticket, selon le type.

5. Vous êtes le demandeur par défaut du ticket. Cependant, en tant que correspondant, vous pouvez ajouter le nom d'un autre utilisateur ou groupe de votre EPLE (si enregistré dans CESAME) dans le cas ou vous enregistrez ce ticket pour une autre personne.

6. Indiquer un (ou plusieurs) utilisateur (ou groupe) intéressé et à informer de la vie du ticket. L'ajout d'utilisateurs supplémentaires est possible qu'après enregistrement du ticket.

7. Indiquer l’utilisateur (ou groupe) qui aura la charge du traitement, qu’il soit interne à l’EPLE ou externe (RIDF, prestataire EPLE,...). Le mieux est de laisser à vide si incertitude et/ou si traitement par la Région/Prestataires.

8.  Indiquer l’urgence de traitement. Elle sera adaptée par le support CESAME suivant la description. Pour information, les tickets signalant un impact sur le réseau sont au minimum **Urgent**. Pour les tickets bureautique, ils sont au maximum **Moyen**. L’impact sera déterminé par le support CESAME selon le type du ticket. La priorité est calculée par l’outil suivant une matrice (urgence + impact), déterminée dans le marché

9. Indiquer la localisation du contexte décrit. Le lieu doit être enregistré sur l'outil afin de pouvoir le sélectionner.

10. Rattacher le (ou les) matériel concerné.

11. Indiquer un titre succinct et clair de votre besoin

12. Décrire un contexte exhaustif de votre souhait. La description pourra être complétée par le support CESAME en cas de complétude. Ou annulé en cas de doublon d'un ticket en cours.

13. Permet de lier votre ticket à un autre en cours de traitement dans votre EPLE

14. ``Ajouter`` : Valider votre ticket (voir dernier point avant de valider)

15. Permet d'ajouter un document aidant au traitement du ticket. Il peut être ajouté avant d'avoir validé votre ticket ou après.  
<sub>[Haut de page](#crationdeticketavecunprofilcorrespondant "Remonter")</sub>


## III. Interagir avec un ticket
Après validation, vous avez une vue du ticket par défaut avec toutes les informations complétées lors de votre création. Vous pouvez interagir sur le ticket en ajoutant un document ou un suivi permettant le traitement de celui-ci. Pour cela vous avez une liste latéral avec des onglets pour sélectionner la bonne rubrique (voir caractéristique ci-dessous).  
<sub>[Haut de page](#crationdeticketavecunprofilcorrespondant "Remonter")</sub>


### a. Présentation du menu latéral
![](./img/liste_lateral_onglets_v2.png)

1. **Ticket** permet de voir la description initiale.

2. **Traitement du ticket** affiche les échanges et actions en rapport avec le ticket. Permet également de compléter le ticket avec des compléments (Les informations sont affichées de manière décroissante, de la plus récente à la plus ancienne) en utilisant le bouton ![](./img/bouton_suivi.png).

3. **Statistique** indique la durée de vie du ticket selon les différentes étapes (attribution) suivant contexte et jusqu’à clôture

4. **Éléments** affiche le matériel/logiciel attaché au ticket et à son contexte. Il permet également d’attacher d’autres éléments.

5. **Tâches de projet** affiche des opérations liés à un projet global auquel est attaché le présent ticket (affectera particulièrement les demandes).

6. **Historique** affiche toutes actions effectuées sur un ticket (log) depuis sa création.

7. **Chronologie** affiche les données de statistiques sous forme de vue graphique dans  le temps.

8. **Livraison** indique les différentes informations sur la livraison de matériels si il y a.

9. **Maintenance** indique un éventuel échange de matériels ou une mise à jour du référentiel

10. **Rendez-vous** Récapitule les dates prévues et effectives

11. **Impression PDF** Permet d’imprimer le ticket visualisé suivant le contexte défini.

12. **Tous** Permet de visualiser l’ensemble des onglets sur une page. N'oubliez pas de sauvegarder avant de cliquer sur **Tous** car cela provoquera un rechargement de contenu dans le navigateur et cela annulera vos modifications apportés au ticket. Contrairement à la sélection unitaire des onglets listés précédemment où il n'y a pas de rechargement de la page.  
<sub>[Haut de page](#crationdeticketavecunprofilcorrespondant "Remonter")</sub>

### b. Annulation/Résolution^[1] du ticket par vos soins.

Pour annuler/résoudre un ticket, il faut vous attribuer le ticket afin d'agir sur celui-ci. Cliquer ensuite sur le bouton ![](./img/bouton_solution.png) pour décrire les actions réalisées et sauvegarder.

Pour s'affecter rapidement le ticket, cliquer sur la **flèche noir** dirigée vers le bas. ![ATTRIB](./img/attribuer_a2.png)

> [1] Cette action n'est pas réalisable avec le profil Personnel de direction.

<sub>[Haut de page](#crationdeticketavecunprofilcorrespondant "Remonter")</sub>

## IIII. Inventaire
xxxxxxxx  
<sub>[Haut de page](#crationdeticketavecunprofilcorrespondant "Remonter")</sub>
