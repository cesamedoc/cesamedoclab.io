# Création de ticket avec un profil Utilisateur
1. [Principe](#iprincipe)
2. [Ouvrir un ticket](#iiouvrirunticket)
3. [Interagir avec un ticket](#iiiinteragiravecunticket)

## I. Principe
CESAME a été mis à disposition afin de signaler des besoins, par la création de ticket incident ou demandes informatique. Par rapport à votre profil, vous pouvez ou non créer un ticket. Pour accéder à l'outil, vous devez disposer d'un compte ENT (MonLycée.net) actif.  
<sub>[Haut de page](#crationdeticketavecunprofilutilisateur "Remonter")</sub>

## II. Ouvrir un ticket
![](./img/Creation_Ticket_Uti.png)

1. Choix du ``type`` de ticket. Incident (Dysfonctionnement, panne, erreur, etc...) ou demande (Renouvellement de matériel, installation, assistance, etc...)

2. Sélectionnez une ``catégorie`` correspondante à votre besoin parmis la liste dans le menu déroulant

3. Spécifiez l'``urgence`` de votre ticket. Ce paramètre pourra être révisé par votre référent  ou par le support CESAME.

4. Permet d'être notifié de l'évolution de votre ticket. L'adresse mél étant celle liée à votre compte CESAME.

5. Indiquez le matériel impacté. Dans le cas où l'inventaire est à jour dans l'outil, vous trouverez les matériels dans la liste déroulante.

6. Ajouter la salle où l'intervention doit avoir lieu. Si les salles sont répertoriées dans l'outil, vous pourrez sélectionner la salle depuis le menu déroulant. Dans le cas contraire ajouter le nom de la salle dans le ``titre`` ou dans la ``description`` de votre besoin.

7. Le ``titre`` doit être explicite avec les informations succinctes de votre besoin

8. ``Description`` : Rédiger votre besoin avec le maximum d'information pour une meilleure prise en charge (Zone, numéro d'étiquette région, lieu, etc...)

9. Cliquer sur le bouton ``parcourir`` afin d'ajouter un document pouvant aider au traitement. Vous avez la possibilité d'ajouter le fichier aussi en glisser/déposer.

10. Validation du ticket  
<sub>[Haut de page](#crationdeticketavecunprofilutilisateur "Remonter")</sub>

## III. Interagir avec un ticket
Vous avez la possibilité d'Interagir sur le ticket en ajoutant un suivi ou un fichier. Pour cela cliquez sur ![](./img/menu_tickets.png) et recherchez le ticket souhaité. Pour la recherche vous avez plusieurs critéres qui vous permetteront de trouver le bon. Dont le numéro du ticket (``ID``) indiqué dans la notification reçu lors de la création de votre ticket.
Pour ajouter un élément au ticket ou consulter l'avancement de clui-ci, cliquez sur l'onglet latéral ![](./img/traitement_du_ticket.png).

### a. Déposer un document
Pour la dépose d'un document cliquez sur le bouton  ![](./img/bouton_document.png) et selectionnez le fichier de votre choix en cliquant sur ``Parcourir``.

### b. Ajout d'un Suivi
Pour l'ajout d'un commentaire ou d'une relance, cliquez sur le bouton ![](./img/bouton_suivi.png).

### c. Solutionner le ticket
Vous avez la possibilité de fermer votre propre ticket si vous considérez que celui-ci est traité en cliquant sur le bouton ![](./img/bouton_solution.png). Descrire les actions réalisées dans le champ prévu à cet effet.

**Attention : Pour fermer un ticket vous devez vous l'attribuer en cliquant sur la flêche noir à coté du + (voir copie d'écran suivante).**

![](./img/attribuer_a2.png)  
<sub>[Haut de page](#crationdeticketavecunprofilutilisateur "Remonter")</sub>
