# Périodes de maintenance de l'outil
<!-- ## xxxxxxxxx 201x - En cours de planification
Coupure d'une journée pour une opération de maintenance des bases de données. -->

## 30 septembre 2019
[&#128279;](#30septembre2019 "Copier le permalien direct vers la maintenance du 30 septembre 2019")
Coupure de service planifiée du lundi 30 septembre 2019 13h00 (*20190930-130000*) au lundi 30 septembre 2019 13h10 (*20190930-131000*), pour opération de maintenance visant le renouvellement de certificat.  
<sub>[^ Haut de page](#priodesdemaintenancedeloutil "Revenir au début de la page")</sub>

## 30 août 2019
[&#128279;](#30aot2019 "Copier le permalien direct vers la maintenance du 30 août 2019")
Coupure de service planifiée du vendredi 30 août 2019 18h00 (*20190830-180000*) au lundi 02 septembre 2019 09h00 (*20190902-090000*), pour opération de maintenance visant des modifications au niveau de l’infrastructure.  
Coupure de service initialement prévue le vendredi 23 août 2019 18h00 (*20190823-180000*).  
<sub>[^ Haut de page](#priodesdemaintenancedeloutil "Revenir au début de la page")</sub>

## 23 août 2019
[&#128279;](#23aot2019 "Copier le permalien direct vers la maintenance du 23 août 2019")
Coupure de service planifiée du vendredi 23 août 2019 18h00 (*20190823-180000*) au lundi 26 août 2019 09h00 (*20190826-090000*), pour opération de maintenance visant des modifications au niveau de l’infrastructure.  
**[Mise à jour 20190823 15h00]** Coupure de service déprogrammée. Opération replanifiée au Vendredi 30 août 2019 18h00 (*20190830-180000*).  
<sub>[^ Haut de page](#priodesdemaintenancedeloutil "Revenir au début de la page")</sub>

## 02 août 2019
[&#128279;](#02aot2019 "Copier le permalien direct vers la maintenance du 02 août 2019")
Coupure de service planifiée du vendredi 02 août 2019 18h00 (*20190802-180000*) au lundi 05 août 2019 15h00 (*20190805-150000*), pour opération de maintenance visant un nettoyage et optimisation DB.  
<sub>[^ Haut de page](#priodesdemaintenancedeloutil "Revenir au début de la page")</sub>

## 24 juillet 2019
[&#128279;](#24juillet2019 "Copier le permalien direct vers la maintenance du 24 juillet 2019")
Coupure de service planifiée entre 19h et 21h, pour opération de maintenance visant une analyse et optimisation DB.  
<sub>[^ Haut de page](#priodesdemaintenancedeloutil "Revenir au début de la page")</sub>

## 11 juillet 2019
[&#128279;](#11juillet2019 "Copier le permalien direct vers la maintenance du 11 juillet 2019")
**Coupure de service toujours en cours**, retour prévu dans l'après-midi.

**[Mise à jour 14h20]** Page de maintenance levée, service rétabli. La maintenance a pris fin à 14h15 ce jour.  
**[Mise à jour 14h40]** Le module de notification fonctionne encore en mode dégradé.  
**[Mise à jour 16h30]** Module de notification rétabli.  
<sub>[^ Haut de page](#priodesdemaintenancedeloutil "Revenir au début de la page")</sub>

## 10 juillet 2019
[&#128279;](#10juillet2019 "Copier le permalien direct vers la maintenance du 10 juillet 2019")
Détérioration des performances et anomalie DB relevée.  
Altération & coupure du service entre 09h20 et 11h.  
Une correction a été apportée par l'hébergeur.

**[Mise à jour 11h30]** **Coupure volontaire urgente du service** par l'hébergeur pour opération de maintenance impérative, conséquence d'anomalies décelées après le premier rétablissement.  
Durée de la coupure estimée à 06 heures, à compter de 11h30.  
<sub>[^ Haut de page](#priodesdemaintenancedeloutil "Revenir au début de la page")</sub>

## 24 juin 2019
[&#128279;](#24juin2019 "Copier le permalien direct vers la maintenance du 24 juin 2019")
Désactivation du plugin `NotificationNavigateur` suite à altération de service. Les autres modes de notifications ne sont pas concernés.  
<sub>[^ Haut de page](#priodesdemaintenancedeloutil "Revenir au début de la page")</sub>

## 04 juin 2019
[&#128279;](#04juin2019 "Copier le permalien direct vers la maintenance du 04 juin 2019")
Dégradation de service en journée liée à une maintenance express, consécutive à fort ralentissements occasionnés par un module tiers de l'outil.  
<sub>[^ Haut de page](#priodesdemaintenancedeloutil "Revenir au début de la page")</sub>

---
## Depuis novembre 2018
[&#128279;](#depuisnovembre2018 "Copier le permalien direct vers la maintenance de novembre 2018")
Ralentissements notables relevés et repérés, conséquences d'opérations lourdes, liés en partie aux usages MCO, aux pratiques (*besoins d'indicateurs*) et à un goulot d'étranglement technique. Modernisation de l'ensemble à venir, dès mise en route du nouveau marché, la situation actuelle (*hébergement&TMA*) étant transitoire. Mise en suspend des opérations massive en semaine.  
<sub>[^ Haut de page](#priodesdemaintenancedeloutil "Revenir au début de la page")</sub>

## 26 octobre 2018
[&#128279;](#26octobre2018 "Copier le permalien direct vers la maintenance du 26 octobre 2018")
Coupure à partir de 14h (*20181026_1400*) jusqu'au **30 octobre 2018** dans le cadre de changement apporté à l'hébergement ainsi qu'au MCO.  
Levée de la maintenance le 30 octobre 2018 à 13h (*20181030_1300*).  
<sub>[^ Haut de page](#priodesdemaintenancedeloutil "Revenir au début de la page")</sub>

## 17 octobre 2018
[&#128279;](#17octobre2018 "Copier le permalien direct vers la maintenance du 17 octobre 2018")
Application des correctifs M13242 et M13272 mineurs, sans coupure de service.
Initialiement prévu au 09 octobre 2018 12h (*20180909_1200*), reporté au 17/10/2018 (*20181017*).  
<sub>[^ Haut de page](#priodesdemaintenancedeloutil "Revenir au début de la page")</sub>

## 30 août 2018
[&#128279;](#30aot2018 "Copier le permalien direct vers la maintenance du 30 août 2018")
Publipostage sur les BAL professionnel des EPLE (*CE, INT et secrétariat*), du calendrier des passages planifiés du TechProx OPT Bureautique attitrés sur 2018-2019.  
<sub>[^ Haut de page](#priodesdemaintenancedeloutil "Revenir au début de la page")</sub>

## 23 avril 2018
[&#128279;](#23avril201 "Copier le permalien direct vers la maintenance du 23 avril 2018")
Coupure d'**une journée** pour mise à jour et montée de version du coeur de l'outil.  
<sub>[^ Haut de page](#priodesdemaintenancedeloutil "Revenir au début de la page")</sub>

## 05 janvier 2018
[&#128279;](#05janvier201 "Copier le permalien direct vers la maintenance du 05 janvier 2018")
Opération de mise à jour base de données, pas d'impact sur la disponibilité du service.  
<sub>[^ Haut de page](#priodesdemaintenancedeloutil "Revenir au début de la page")</sub>

---
## 01 juin 2017
[&#128279;](#01juin201 "Copier le permalien direct vers la maintenance du 01 juin 2017")
Mise en production du plugin SLM, pas d'impact sur la disponibilité du service.  
<sub>[^ Haut de page](#priodesdemaintenancedeloutil "Revenir au début de la page")</sub>

## 03 avril 2017
[&#128279;](#03avril201 "Copier le permalien direct vers la maintenance du 03 avril 2017")
Coupure d'**une journée** pour permettre la mise à jour et montée de version du coeur de l'outil.  
<sub>[^ Haut de page](#priodesdemaintenancedeloutil "Revenir au début de la page")</sub>

---
<sub>
<!-- <table class="table tborder"> -->
 <caption></caption>
 <tbody>
  <tr>
   <th scope="col">Rédaction</td>
   <th scope="col">Contribution</td>
   <th scope="col">Version/Dernière mise à jour<br>Article</td>
  </tr>
  <tr>
   <th scope="row">IBE.CESAME</th>
   <td>IBE.CESAME</td>
   <td>20190823</td>
  </tr>
 </tbody>
</table>
</sub>
