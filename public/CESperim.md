[Matrice_MCOPerim_Zone]: ./img/Graph_CESMCOPerim_Zones.png "RIDF/CES Matrice des Vue de la matrice des zone MCO en EPLE"
[Matrice_MCOPerim_SRV]: ./img/Graph_CESMCOPerim_SRV.png "RIDF/CES Matrice des périmètre MCO Serveur en EPLE"
[Matrice_MCOPerim_RES]: ./img/Graph_CESMCOPerim_RES.png "RIDF/CES Matrice des périmètre MCO Réseau en EPLE"
[Matrice_MCOPerim_SVCPeda]: ./img/Graph_CESMCOPerim_SVCPeda.png "RIDF/CES Matrice des périmètre MCO Services/Rôles pédagogique en EPLE"
[Matrice_MCOPerim_SVCAdmn]: ./img/Graph_CESMCOPerim_SVCAdmn.png "RIDF/CES Matrice des périmètre MCO Services/Rôles administratif en EPLE"
[Matrice_MCOPerim_BURPeda]: ./img/Graph_CESMCOPerim_BURPeda.png "RIDF/CES Matrice des périmètre MCO Bureautique pédagogique en EPLE"
[Matrice_MCOPerim_BURAdmn]: ./img/Graph_CESMCOPerim_BURAdmn.png "RIDF/CES Matrice des périmètre MCO Bureautique administrative en EPLE"

# Périmètres
<sup>[&#128279;](#primtres "Copier le permalien direct de la section intitulée Périmètres")</sup>Les actions des intervenants (*Académique ou Région*) sont cadrées par des périmètres de prise en charge (*Autorité & compétence*), également délimités par une notion de zone (*Autorité*).

<!--La coordination et synchronisation des interventions DSI AC et [RIDF](Définition "Région Île-de-France"), nos institutions ont conscience de ce point et qu'une amélioration est nécessaire, à travailler. Ce dossier est en cours, et comprends la nécessité de mettre en place une convention et des procédures. La Région devant travailler sur ce dossier avec 3 DSI Académique distinctes, avec leurs spécificités et contraintes propres.-->
<!--[Exemple](https://cesame.iledefrance.fr/front/ticket.form.php?id=45355)-->

Notons que, dans le cadre de l'autonomie des Lycées et <u>s'il possède les compétences, ainsi que les ressources</u>, l'[EPLE](Définition "Établissement Public Local d'Enseignement") est à même d'exploiter lui même les différents périmètres de son parc informatique dans le cadre de l'autonomie.

1. [Zone Pédagogique](#izonepdagogique "Accéder à la section portant sur la zone pédagogique")
2. [Zone Administrative](#iizoneadministrative "Accéder à la section portant sur la zone Administrative")
3. [Définition des zones](#iiidfinitiondeszones "Accéder à la section portant sur la délimitation des zones en EPLE")
4. [Cas particulier](#iiiicasparticulier "Accéder à la section portant sur les cas particuliers")
5. [Tableaux récapitulatifs](#vtableauxrcapitulatifs "Accéder à la section présentant des tableaux récapitulatifs")
  - 5.a. [Serveur](#vaserveur "Accéder à la sous-section portant sur les délimitations du périmètre serveur")
  - 5.b. [Réseau](#vbrseau "Accéder à la sous-section portant sur les délimitations du périmètre réseau")
  - 5.c. [Service/Rôle - Zone pédagogique](#vcservicerlezonepdagogique "Accéder à la sous-section portant sur les délimitations du périmètre des services en zone pédagogique")
  - 5.d. [Service/Rôle - Zone administrative](#vdservicerlezoneadministrative "Accéder à la sous-section portant sur les délimitations du périmètre des services en zone administrative")
  - 5.e. [Bureautique - Zone pédagogique](#vebureautiquezonepdagogique "Accéder à la sous-section portant sur les délimitations du périmètre bureautique en zone pédagogique")
  - 5.f. [Bureautique - Zone administrative](#vfbureautiquezoneadministrative "Accéder à la sous-section portant sur les délimitations du périmètre bureautique en zone administrative")


Nous rappelons que la formation des personnels du Lycée ne fait pas partie des compétences ou du périmètre de la Région Île-de-France (*RIDF*) et qu'aucune formation n'est proposée au catalogue.

L'[EPLE](Définition "Établissement Public Local d'Enseignement") est invité à émettre sa demande de formation dans le cadre du Plan Académique de Formation ([*PAF*]((http://eduscol.education.fr/cid46777/plans-academiques-formation.html "Accéder à la page du PAF de l'éducation nationale")) de l'éducation nationale.  
<sup>(*En savoir plus / Voir également la [formation continue pour les personnels du ministère de l'éducation nationale](http://www.education.gouv.fr/cid1104/la-formation-continue-pour-les-personnels-ministere.html#Les_plans%20acad%C3%A9miques%20de%20formation "Accéder à la page dédiée à la formation continue des personnels du ministère de l'éducation nationale")*)</sup>


## I. Zone pédagogique
<sup>[&#128279;](#izonepdagogique "Copier le permalien direct de la section intitulée Zone pédagogique")</sup>
La [RIDF](Définition "Région Île-de-France"), incluant ses prestataires titulaires de marchés (*MCO, Fourniture,..*), intervient sur la bureautique (*Ordinateur, Imprimantes, VP*), les serveurs, onduleurs, éléments actifs du réseau (*Commutateurs, Borne WiFi, ...*).

Pour toute commande de matériel bureautique, les [EPLE](Définition "Établissement Public Local d'Enseignement") sont invités à utiliser l'outil [LyStore](https://ent.iledefrance.fr/lystore "Accéder à LyStore") (*voir la [documentation en ligne](https://lystoredoc.gitlab.io "Accéder à la documentation en ligne de LyStore")*) de la [RIDF](Définition "Région Île-de-France"), disponible selon ouverture de campagne.

L'administration, exploitation et utilisation quotidienne des serveurs (*comptes utilisateurs, ACL, répertoires, ...*) n'entrent pas dans le cadre du Maintien en Condition Opérationnelle (*MCO*) de la [RIDF](Définition "Région Île-de-France").

<u>L'acquisition, l'installation, le maintien et l'exploitation des applications (*logiciels*), hors système d'exploitation, est à la charge de l'[EPLE](Définition "Établissement Public Local d'Enseignement"), ainsi que de l'académie et ne relèvent pas des compétences de la [RIDF](Définition "Région Île-de-France").</u>
<!--Toutefois, sous certaines conditions, comme la fourniture d'une procédure complète, et uniquement sur validation de la [RIDF](Définition "Région Île-de-France"), cette dernière pourrait dépécher une mission d'assistance, ou de déploiement-->  
<sub>[^ Haut de page](#primtres "Revenir au début de la page")</sub>

## II. Zone administrative
<sup>[&#128279;](#iizoneadministrative "Copier le permalien direct de la section intitulée Zone administrative")</sup>
L'ensemble de la zone administrative est sous autorité de la DSI du rectorat Académique et n'entre pas dans la portée (*scope*) du maintien informatique de la [RIDF](Définition "Région Île-de-France"), incluant ses prestataires.  
Par conséquent, la [RIDF](Définition "Région Île-de-France") n'a ni compétence ni autorité pour agir sur cette zone et les périmètres quelle comprend (*même pour un simple redémarrage*).  

**Exemple** :
 - Passerelle de communication [EOLE/AMON &#8599;](https://pcll.ac-dijon.fr/eole "Accéder au site de la distribution Linux Ensemble Ouvert Libre Évolutif")
 - Serveur [EOLE/HORUS &#8599;](https://pcll.ac-dijon.fr/eole "Accéder au site de la distribution Linux Ensemble Ouvert Libre Évolutif")
 - Postes clients
 - Site internet académique de l'[EPLE](Définition "Établissement Public Local d'Enseignement")
 - ...

<u>Les applications et logiciels sont à la charge de l'[EPLE](Définition "Établissement Public Local d'Enseignement") et de l'Académie, via la DSI du rectorat de l'académie.</u>


**Exception** :
- Dans le cadre d'**anomalie réseau**, et **après une première intervention de la DSI AC** qui écarte tout autre source de dysfonctionnement que l'infrastructure réseau, **la [RIDF](Définition "Région Île-de-France") peut intervenir pour rétablir l'accès qui aurait pour cause une anomalie au niveau des actifs réseaux**.
- Après signalement de l'état anormal d'un matériel doté par la [RIDF](Définition "Région Île-de-France"), accompagné d'une validation motivée du besoin de renouvellement <u>par la DSI du rectorat Académique</u>, la [RIDF](Définition "Région Île-de-France") fournira le matériel (*Serveur, Réseau, Bureautique, ...*) de remplacement, sans opérer l'installation, ni la mise en service, qui sont assurés par la DSI du rectorat.  
<sub>[^ Haut de page](#primtres "Revenir au début de la page")</sub>

## III. Définition des zones
<sup>[&#128279;](#iiidfinitiondeszones "Copier le permalien direct de la section intitulée Définition des zones")</sup>
![Vue de la matrice des zones MCO][Matrice_MCOPerim_Zone]  
<sub>[^ Haut de page](#primtres "Revenir au début de la page")</sub>

## IIII. Cas particulier
<sup>[&#128279;](#iiiicasparticulier "Copier le permalien direct de la section intitulée Cas particulier")</sup>
La solution antivirus sur la zone pédagogique est installée et dépoyée par la [RIDF](Définition "Région Île-de-France"). Cependant, la clé de licence est obtenue auprès de la DSI Académique.  

Lors d'anomalie de la connexion internet il est important d'alerter la [RIDF](Définition "Région Île-de-France") (*qui écartera un dysfonctionnement infrastructure*) ainsi que la DSI académique (*qui pourra vérifier l'état de la passerelle de communication à distance*).

Les dysfonctionnements réseaux peuvent être traités par la [RIDF](Définition "Région Île-de-France") sur la zone administrative après une première intervention de la DSI AC qui aura écarté toute autre anomalie.

<sub>[^ Haut de page](#primtres "Revenir au début de la page")</sub>

## V. Tableaux récapitulatifs
<sup>[&#128279;](#vtableauxrcapitulatifs "Copier le permalien direct de la section intitulée Tableaux récapitulatifs")</sup>
Vous trouverez ci-desous une suite de représentation simplifiée des périmètres de MCO, en notant que davantage d'informations sont présentes dans l'outil CESAME.  
<sub>[^ Haut de page](#primtres "Revenir au début de la page")</sub>

### V.a. Serveur
<sup>[&#128279;](#vaserveur "Copier le permalien direct de la sous-section intitulée Serveur")</sup>
![Vue de la matrice des périmètre MCO Serveur][Matrice_MCOPerim_SRV]  
<sub>[^ Haut de page](#primtres "Revenir au début de la page")</sub>

### V.b. Réseau
<sup>[&#128279;](#vbrseau "Copier le permalien direct de la sous-section intitulée Réseau")</sup>
![Vue de la matrice des périmètre MCO réseau][Matrice_MCOPerim_RES]  
<sub>[^ Haut de page](#primtres "Revenir au début de la page")</sub>

### V.c. Service/Rôle - Zone pédagogique
<sup>[&#128279;](#vcservicerlezonepdagogique "Copier le permalien direct de la sous-section intitulée Services en zone pédagogique")</sup>
![Vue de la matrice des périmètre MCO Service pédagogique][Matrice_MCOPerim_SVCPeda]  
<sub>[^ Haut de page](#primtres "Revenir au début de la page")</sub>

### V.d. Service/Rôle - Zone administrative
<sup>[&#128279;](#vdservicerlezoneadministrative "Copier le permalien direct de la sous-section intitulée Services en zone Administrative")</sup>
![Vue de la matrice des périmètre MCO réseau][Matrice_MCOPerim_SVCAdmn]  
<sub>[^ Haut de page](#primtres "Revenir au début de la page")</sub>

### V.e. Bureautique - Zone pédagogique
<sup>[&#128279;](#vebureautiquezonepdagogique "Copier le permalien direct de la sous-section intitulée Bureautique en zone pédagogique")</sup>
![Vue de la matrice des périmètre MCO réseau][Matrice_MCOPerim_BURPeda]  
<sub>[^ Haut de page](#primtres "Revenir au début de la page")</sub>

### V.f. Bureautique - Zone administrative
<sup>[&#128279;](#vfbureautiquezoneadministrative "Copier le permalien direct de la sous-section intitulée Bureautique en zone pédagogique")</sup>
![Vue de la matrice des périmètre MCO réseau][Matrice_MCOPerim_BURAdmn]  
<sub>[^ Haut de page](#primtres "Revenir au début de la page")</sub>

---
<sub>
<table class="table">
 <caption></caption>
 <tbody>
  <tr>
   <th scope="col">Rédaction</td>
   <th scope="col">Contribution</td>
   <th scope="col">Version/Dernière mise à jour<br>Article</td>
  </tr>
  <tr>
   <th scope="row">IBE.CESAME</th>
   <td>IBE.CESAME</td>
   <td>20190829</td>
  </tr>
 </tbody>
</table>
</sub>
