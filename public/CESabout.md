[Aiguillage]: I-Aiguillage "L'orientation"
[Traitement]: II-Traitement "Traitement du dossier MCO"
[Fermeture]: III-Fermeture-de-dossier "Fin du dossier"
[Graph]: IV-Repr-sentation-graphique "Graphique"
[Process_MCO]: ./img/Graph_CES_MCO_Simple.png "Processus MCO RIDF CESAME simplifié"

# À propos
<sup>[&#128279;](#propos "Copier le permalien direct de la section intitulée À propos")</sup>CESAME<sup>[1]</sup> permet aux lycées franciliens d'exprimer leurs besoins d'évolution et de déclarer des incidents par le biais de ticket.  
CESAME est composé d'un outil basé sur GLPI et suivi par une équipe support dédiée.  
L'outil met également à disposition une [base de connaissance &#8599;](https://cesame.iledefrance.fr/front/knowbaseitem.php "Accéder à la base de connaissance, profil Correspondant & PerDir") liée au Maintien en Condition Opérationnelle ([*MCO*](Définition "Maintien en Condition Opérationnelle")) de la Région. Attention, <u>pour les utilisateurs de l'interface simplifiée</u>, l'accès se fait par la rubrique [FAQ &#8599;](https://cesame.iledefrance.fr/front/helpdesk.faq.php "Accéder à la FAQ, profil utilisateur final") de l'outil.

> <sup>[1]</sup> <sub>CESAME : [Acronyme](Définition "Une des typologie de système abréviatifs, donnant lieu à une abréviation dont le résultat forme un mot prononcé sans l'épeler") pour Centre de Service et d'Accompagnement pour la Maintien des Equipements de la Région</sub>

## Simplicité d'accès
<sup>[&#128279;](#simplicitdaccs "Copier le permalien direct de la section intitulée Simplicité d'accès")</sup>L'outil est accessible depuis le réseau social éducatif [MonLycée.net &#8599;](https://monlycee.net "Accéder au site d'information monlycée.net"), pas d'identifiant supplémentaire nécessaire pour les personnels du lycée (*Administrifs ou éducatifs*).  
En cas de difficulté d'accès à l'[ENT MonLycée.net](https://ent.iledefrance.fr "Accéder à l'ENT") nous invitons les utilisateurs à consulter les guides d'[initialisation MonLycée.net &#8599;](http://monlycee.net/pages/guides "Accéder aux guides"). <sup>Exemple avec la fiche comment [activer mon compte et faire mes premiers pas dans l'ENT &#8599;](http://monlycee.net/files/guides/Activer_mon_compte_et_faire_mes_premiers_pas_dans_lENT.pdf "Accéder au document PDF")


## Ouvert à tout le personnel présent dans l'ENT
<sup>[&#128279;](#ouverttoutlepersonnelprsentdanslent "Copier le permalien direct de la section intitulée Ouvert à tout le personnel présent dans l'ENT")</sup>Dans le cadre de son autonomie, la décision d'autoriser l'utilisation de CESAME à l'ensemble (*ou une partie*) de son personnel éducatif reste du ressort de l'EPLE, qui peut ouvrir cet accès depuis la console d'administration de l'ENT ([MonLycée.net &#8599;](https://en.iledefrance.fr)).

CESAME peut accueillir les déclarations de tous les besoins informatique de l'EPLE, même ceux traités en interne.  
L'EPLE a la possibilité de traiter tout ou partie des besoins exprimés par son personnel, ou de transmettre tout ou partie des besoins exprimés auprès de la RIDF.

Il est également possible de créer une règle automatique pour positioner le correspondant CESAME de l'EPLE comme valideur de tous besoins qu'exprime son EPLE d'affectation.

<table class="table tborder">
 <caption>Exemples de scenarios possibles pour les déclarations et traitements dans CESAME</caption>
 <tbody>
  <tr>
   <th scope="col">#</td>
   <th scope="col">Scénario</td>
   <th scope="col">Avantage</td>
   <th scope="col">Inconvénient</td>
  </tr>
  <tr>
   <th scope="row">1</th>
   <td>- L'EPLE ne possède pas de ressource interne.<br>- Un correspondant CESAME centralise les besoins de ses collègues.<br>- Il valide les besoins.<br>- Il déclare les besoins qui sont adressés à la RIDF sur CESAME.</td>
   <td>- Vue directe et d'ensemble des besoins.<br>- Evite les doublons ou les erreurs.</td>
   <td>- Gouleau d'étranglement potentiel, lié au volume de saisie généré pour une personne, qui se retrouve point d'entrée unique. Ce point peut devenir bloquant en cas d'absence de la personne.<br>- Un remplacant doit être désigné pour palier aux absences.<br>- Coordination entre les responsables</td>
  </tr>
  <tr>
   <th scope="row">2</th>
   <td>- L'EPLE ne possède pas de ressource interne.<br>- Plusieurs référents (<em>par matière, batiment, ... selon contexte EPLE</em>) centralisent les besoins de leurs collègues.<br>- Ils déclarent sur CESAME.<br>- Le Correspondant CESAME valide les besoins qui sont adressés à la RIDF.</td>
   <td>- Charge de création répartie sur plusieurs acteurs.<br>- Vue directe et d'ensemble des besoins.<br>- Evite les doublons ou erreurs.<br>- Plusieurs niveaux de validation.</td>
   <td>- Génère de la saisie à chaque point d'entrée.<br>- Un remplacant doit être désigné pour palier aux absences.<br>- Coordination entre les responsables</td>
  </tr>
  <tr>
   <th scope="row">3</th>
   <td>- L'EPLE ne possède pas de ressource interne.<br>- L'ensemble des personnels éducatifs déclarent les besoins sur CESAME.<br>- Le correspondant, vue avec les référents ou non, valide le traitement des besoins qui sont adressés à la RIDF.</td>
   <td>- Charge de création répartie sur l'ensemble des personnels.<br>- Vue directe et d'ensemble des besoins.<br>- Evite les doublons ou erreurs.<br>- Plusieurs niveaux de validations.<br>- L'EPLE et la RIDF dispose d'une vue d'ensemble des traitements et d'une meilleure connaissance de contexte (<em>Incident, Evolution</em>).</td>
   <td>- Le valideur (<em>correspondant</em>) doit être secondé.<br>- Un remplacant doit être désigné pour palier aux absences.<br>- Coordination entre les responsables.</td>
  </tr>
  <tr>
   <th scope="row">4</th>
   <td>- L'EPLE possède de la ressource technique à même d'intervenir (<em>Interne ou prestataire fonds propres</em>).<br>- Le personnel éducatif déclare les besoins sur CESAME.<br>- Le correspondant, vue avec les référents ou non, valide le traitement des besoins qui sont traités en interne ou qui sont adressés à la RIDF.</td>
   <td>- Charge de création répartie sur tous les acteurs.<br>- Vue directe et d'ensemble des besoins.<br>- Evite les doublons ou erreurs.<br>- Plusieurs niveaux de validation.<br>- L'EPLE et la RIDF dispose d'une vue d'ensemble des traitements et d'une meilleure connaissance de contexte (<em>Incident, Evolution</em>).<br>- L'EPLE suit les traitements pris en interne ou ce qui est traité par la RIDF.</td>
   <td>- Le valideur (<em>correspondant</em>) doit être secondé.<br>- Un remplacant doit être désigné pour palier aux absences.<br>- Coordination entre les responsables</td>
  </tr>
 </tbody>
</table>

<span style="color: #ba0000;">**Attention**</span>  
Selon le scenario en place dans l'EPLE, lorsqu'un ticket est créé et qu'il ne doit pas être traité en interne, il est impératif de <u>ne surtout pas vous attribuer le ticket</u> [ou à l'un des personnels de l'EPLE].  
Notez que dans une telle situation (*attribution d'un ticket créé à un acteur EPLE*), le dossier  disparaitrait de la bannette de traitement et de visibilité du support CESAME, et ne sera par conséquent pas pris en charge [, tout du moins dans l'immédiat] et risque d'être traité avec du retard.  
Cette erreur d'affectation empêchera la prise en charge et le traitement du dossier dans un temps convenable par les services de la Région.

<sub>[^ Haut de page](#propos "Revenir au début de la page")</sub>

## Expression du besoin
<sup>[&#128279;](#expressiondubesoin "Copier le permalien direct de la section intitulée Expression du besoin")</sup><u>Cette étape initiale est primordiale et conditionne tout le déroulé, ainsi que la vie, du dossier.</u>  
Un besoin exprimé doit être rédigé de manière clair par l'EPLE, avec tous les détails jugés nécessaires. <u>A défaut, le support CESAME exigera un complément d'information auprès du demandeur initial (*EPLE*).</u>  
**<span style="color: #ba0000;">Attention</span>, un ticket contient impérativement <u>un</u> besoin clair unique, limité à <u>un</u> périmètre.  
Il ne doit en aucun cas comporter plusieurs besoins, sur plusieurs périmètres.**

<span style="color: #ba0000;">**1 besoin logique = 1 ticket**</span>

Notez qu'à terme, tout ticket créé avec expression de besoin multiples et périmètres divers sera clos.  
<sub>[^ Haut de page](#propos "Revenir au début de la page")</sub>

---

Une fois qu'il a été exprimé sur l'outil CESAME, le besoin passe par différentes étapes avant d'arriver à  terme :
1. [Aiguillage](#iaiguillage "Accéder à la section Aiguillage")
2. [Traitement](#iitraitement "Accéder à la section Traitement")
3. [Fermeture de dossier](#iiifermeturededossier "Accéder à la section Fermeture de dossier")

Pour terminer, vous pouvez visualiser une vue graphique qui synthétise le circuit de traitement d'un ticket.
4. [Représentation graphique](#iiiireprsentationgraphique "Accéder à la section Représentation graphique")

Attention toutefois à ne pas prendre les délais présentés au pied de la lettre, chaque besoin étant un cas particulier, les délais sont amenés à varier.  
<sub>[^ Haut de page](#propos "Revenir au début de la page")</sub>

---

# I. Aiguillage
<sup>[&#128279;](#iaiguillage "Copier le permalien direct de la section intitulée Aiguillage")</sup>Un ticket ouvert est réceptionné par l'équipe support CESAME qui est chargée d'analyser celui-ci, puis d'orienter le dossier vers le bon interlocuteur, qu'il soit interne ou externe.

La prise en charge est rapide mais l'orientation peut être ralentie si le dossier est considéré comme incomplet à la qualification (*analyse initiale*).  
Un dossier est incomplet dès lors qu'il ne permet pas de cibler précisement un [périmètre d'action &#8599;](/CESperim.html "Accéder à la page des Périmètres du MCO Informatique RIDF") et qu'il ne peut être orienté vers le bon interlocuteur.  
Il est alors placé en attente et une demande de complément est adressée au Lycée.  
<span style="color: #ba0000;">Notez qu'<u>après 3 relances infructueuses, sans réponse de votre part, le dossier est fermé</u>.</span>


**Exemple**
```
Ticket exprime le besoin de déployer des ordinateurs dans le lycée, sans autre précision.

Information(s) manquante(s) :
 - Salle(s) visée(s) ?
 - Nombre de poste
 - Référence de poste (N° d'étiquette, SN, PN)
 - Répartition par salle
 - Zone (Administrative/Pédagogique)
 - Installation d'outils tiers
 - ...

Sans ces informations l'interlocuteur visé par ce périmètre n'est pas à même de commander les prestations adéquates.
```
L'interlocuteur vers qui doit être orienté un dossier est fonction du contexte décrit par l'EPLE et du [périmètre &#8599;](/CESperim.html "Accéder à la page des Périmètres du MCO Informatique RIDF") touché.  
<sub>[^ Haut de page](#propos "Revenir au début de la page")</sub>

# II. Traitement
<sup>[&#128279;](#iitraitement "Copier le permalien direct de la section intitulée Traitement")</sup>Selon le périmètre décrit dans le dossier et l'ensemble des informations récoltées, cette partie sera plus ou moins longue.

Les périmètres ne sont pas traités sur la même échelle de temps.

## Type de besoin : Demande
Une demande simple pourra mettre moins d'une semaine à être réalisée lorsqu'une demande complexe pourra mettre jusqu'à plusieurs mois.
La différence s'explique par le nombre d'interlocuteurs qui entrent en jeux, par le délai de chacune des interventions et par leurs degrés d'entrelacement ou de complexité.  
- **Exemple d'une demande simple :** `Mise à jour d'un VLAN`, ou bien le `renouvellement d'un serveur sans changement majeur (ISO fonctionnel)`
- **Exemple d'une demande complexe :** `Câblage et activation réseau d'un batîment en rénovation`, ou encore `Couverture WiFi sans câblage réseau`  

## Type de besoin : Incident
Une anomalies n'est pas traitée comme un besoin de type demande (*évolution*) et est soumise à des délais de traitement prévus au marché, comme par exemple `J+1`, `J+3` ou `J+5` selon la priorité (*gravité*) du dossier (*qui est soumise par le demandeur et réévalué par le support lors de la qualification*).  
En cas de dépassement du délai de traitement par le prestataire, celui-ci encours le risque de l'application d'une pénalité selon les règles prévues au marché.




Notez que les prestataires dépéchés par la Région agissent dans le cadre de marché RIDF en appliquant les actions & stratégies selon directives & réferentiels de la RIDF (*PLY/STN*).

<sub>[^ Haut de page](#propos "Revenir au début de la page")</sub>

# III. Fermeture de dossier
<sup>[&#128279;](#iiifermeturededossier "Copier le permalien direct de la section intitulée Fermeture de dossier")</sup>Le traitement arrivé à terme, le dossier passe au statut résolu.  
Le demandeur dispose alors de 30 jours ouvrés pour manifester un désaccord <u>motivé</u>, <u>en corrélation avec le dossier</u>, et demander à ce que celui-ci soit réévalué.  
Cette information est rappelée dans chaque notification mél de fermeture (*solution*) de ticket.  
<sub>[^ Haut de page](#propos "Revenir au début de la page")</sub>

# IIII. Représentation graphique
<sup>[&#128279;](#iiiireprsentationgraphique "Copier le permalien direct de la section intitulée Représentation graphique")</sup>![Vue graphique process MCO][Process_MCO]  
<sub>[^ Haut de page](#propos "Revenir au début de la page")</sub>

---
<sub>
<!-- <table class="table tborder"> -->
 <caption></caption>
 <tbody>
  <tr>
   <th scope="col">Rédaction</td>
   <th scope="col">Contribution</td>
   <th scope="col">Version/Dernière mise à jour<br>Article</td>
  </tr>
  <tr>
   <th scope="row">IBE.CESAME</th>
   <td>IBE.CESAME</td>
   <td>20190823</td>
  </tr>
 </tbody>
</table>
</sub>
