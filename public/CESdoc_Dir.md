# Création de ticket avec un profil Personnel de Direction
1. [Principe](#iprincipe)
2. [Présentation de la page d'accueil](#iiprsentationdelapagedaccueil)
3. [Présentation du menu latéral](#iiiprsentationdumenulatral)
4. [Observer un ticket](#iiiiobserverunticket)

## I. Principe
Pour accéder à l'outil CESAME, Vous devez disposer d'un compte ENT (MonLycée.net) actif. Le profil PerDIR ne dispose pas des droits pour créer des tickets mais permet la visualisation des tickets présents sur votre EPLE. Le profil ne permet pas l'ajout de matériels mais de visualiser l'ensemble du parc. La modification d'une fiche d'un matériel reste possible.  
<sub>[Haut de page](#crationdeticketavecunprofilpersonneldedirection "Remonter")</sub>


## II. Présentation de la page d'accueil
Pour chaque profil l'outil définie une page par défaut par rapport à vos droits. Vous trouverez ci-dessous la présentation de la page d'accueil de votre profil.

![](img/page_Accueil_PERDIR.png)
1. **Menu utilisateur** :
 - La loupe permet une recherche par mot clé
 - Le "?"" permet d’accéder à l’aide
 - "L'étoile" permet de visionner vos vues personnalisées enregistrées dans l'outil
 - "L'engrenage" ou le "Nom utilisateur" permet de choisir vos préférences personnelles (ex: ajout d'un numéro de téléphone)
 - Le "bouton" permet de se déconnecter

2. **Parc** : Affiche la vue inventaire des Ordinateurs par défaut. Plusieurs  critères de recherche, basés sur les champs de la fiche matériel, sont à définir. Tous matériels inventoriés et enregistré dans l'outil peuvent être visibles par rapport à votre filtre. En cliquant sur l'``étoile`` vous avez la possibilité d'enregistrer votre recherche validée.

3. **Assistance** : Affiche la vue générale des tickets. Permet de rechercher des tickets sur votre EPLE pour interagir sur des tickets "En cours", "En attente", ou "Résolu". Les tickets "Clos" ne peuvent pas être rouvert ou modifiés.

4. **Gestion** : Affiche les licences de votre EPLE enregistrés dans l'outil.

5. **Outils** Affiche la vue des projets par défaut. Le menu déroulant du module vous donne d'autre droits comme vos notes personnelles par exemple

6. **Configuration** : Permet de visualiser les informations générales sur votre EPLE. Exemple : IP, nom de domaine, etc...

7. **Barre des vues** : Par défaut la vue est sur "vue personnelle". Vous avez la possibilité de filtrer ces vues afin de trouver plus facilement vos tickets sur la page d'accueil.

8. **Notes personnelles** : Ce module vous permet de vous faire des notes personnelles en cliquant sur le ``+``.

9. **Notes publiques** : Affiche des informations diffusées par la Région  
<sub>[Haut de page](#crationdeticketavecunprofilpersonneldedirection "Remonter")</sub>


## III. Présentation du menu latéral
Onglets côté gauche du ticket
![](./img/liste_lateral_onglets.png)
1. **Ticket** permet de voir la description initiale.

2. **Traitement du ticket** affiche les échanges et actions en rapport avec le ticket. Permet également de compléter le ticket avec des compléments (Les informations sont affichées de manière décroissante, de la plus récente à la plus ancienne) en utilisant le bouton **Suivi**

3. **Statistique** indique la durée de vie du ticket selon les différentes étapes (attribution) suivant contexte et jusqu’à clôture

4. **Eléments** affiche le matériel/logiciel attaché au ticket et à son contexte. Il permet également d’attacher d’autres éléments.

5. **Tâches de projet** affiche des opérations liés à un projet global auquel est attaché le présent ticket (affectera particulièrement les demandes).

6. **Historique** affiche toutes actions effectuées sur un ticket (log) depuis sa création.

7. **Chronologie** affiche les données de statistiques sous forme de vue graphique dans  le temps.

8. **Livraison** indique les différentes informations sur la livraison de matériels si il y a.

9. **Maintenance** indique un éventuel échange de matériels ou une mise à jour du référentiel

10. **Rendez-vous** Récapitule les dates prévues et effectives

11. **Impression PDF** Permet d’imprimer le ticket visualisé suivant le contexte défini.

12. **Tous** Permet de visualiser l’ensemble des onglets sur une page. N'oubliez pas de sauvegarder avant de cliquer sur **Tous** car cela provoquera un rechargement de contenu dans le navigateur et cela annulera vos modifications apportés au ticket. Contrairement à la sélection unitaire des onglets listés précédemment, où il n'y a pas de rechargement de la page.  
<sub>[Haut de page](#crationdeticketavecunprofilpersonneldedirection "Remonter")</sub>


## IIII. Observer un ticket
Pour s'ajouter en tant qu'observateur sur un ticket, il faut cliquer sur la **flèche noir** à côté de **Observateur** (voir copie d'écran ci-dessous).

![](./img/ajout_observateur.png)  
<sub>[Haut de page](#crationdeticketavecunprofilpersonneldedirection "Remonter")</sub>
