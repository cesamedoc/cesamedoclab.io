# Présentation de l'interface
1. [Principe](#iprincipe)
2. [Vue d'un utilisateur: Interface simple](#iivueutilisateurinterfacesimple)
3. [Vue d'un correspondant ou d'un Personnel de Direction](#iiivuecorrespondantoupersonneldedirectioninterfacestandard)
4. [Récupérer son accès ENT MonLycee.net](#iiiircuprersonaccsentmonlyceenet)

## I. Principe
A la connexion  
L'interface proposée à l'utilisateur diffère selon l'habilitation de celui-ci dans l'outil.
Celle-ci peut être de 2 types :
1. Standard
2. Simple

Selon que vous soyez Utilisateur, Correspondant ou Personnel de Direction, la vue par défaut à l'ouverture de l'outil ne sera pas la même.
Les utilisateurs verront l'interface Simple.
Les **Correspondant** et **personnelle de direction** verront l'interface standard, avec quelques petite différence d'option.  
<sub>[Haut de page](#prsentationdelinterface "Remonter")</sub>

## II. Vue utilisateur : Interface simple
Pour le profil **Utilisateur** lors de votre connexion à l'outil, votre page par défaut sera sur `Créer un ticket`. Vous pouvez créer des tickets, les visualiser, ajouter des informations sur un ticket (en cours, en attente ou résolu). Par contre vous n'avez pas le droit à l'inventaire de votre lycée.

![page_par_defaut_uti](./img/page_par_defaut_uti.png)

1. `Accueil` Permet de visualiser l’ensemble des tickets créés à l’aide d’un tableau récapitulatif succinct. Ainsi que des informations sur la plateforme.

2. `Créer un ticket` : Permet de procéder à la rédaction de votre requête.

3. `Ticket` : Permet de visualiser l’ensemble de vos requêtes grâce à une page détaillée. Celle-ci est ajustable  à  l’aide d’un  filtre d’affichage, qui par défaut est paramétré pour afficher les « Statut de ticket : Non résolu ».

4. `Foire au question` : Permet d’accéder à la base de connaissance enregistrée dans l’outil. Vous permet d’avoir des réponses sur certains points technique précis. Cette section est encore en construction et est donc incomplète.

5. Choix du `type` de ticket. Incident (Dysfonctionnement, panne, erreur, etc...) ou demande (Renouvellement de matériel, installation, assistance, etc...)

6. Sélectionnez une `catégorie` correspondante à votre besoin parmis la liste dans le menu déroulant

7. Spécifiez l'`urgence` de votre ticket. Ce paramètre pourra être révisé par votre référent  ou par le support CESAME.

8. Permet d'être notifié de l'évolution de votre ticket. L'adresse mél étant celle liée à votre compte CESAME.

9. Indiquez le matériel impacté. Dans le cas où l'inventaire est à jour dans l'outil, vous trouverez les matériels dans la liste déroulante.

10. Ajouter la salle où l'intervention doit avoir lieu. Si les salles sont répertoriées dans l'outil, vous pourrez sélectionner la salle depuis le menu déroulant. Dans le cas contraire ajouter le nom de la salle dans le `titre` ou dans la `description` de votre besoin.

11. Le `titre` doit être explicite avec les informations succinctes de votre besoin

12. Rédiger votre besoin avec le maximum d'information pour une meilleure prise en charge (Zone, numéro d'étiquette région, lieu, etc...)

13. Cliquer sur le bouton `parcourir` afin d'ajouter un document pouvant aider au traitement. Vous avez la possibilité d'ajouter le fichier aussi en glisser/déposer.

14. Validation du ticket  
<sub>[Haut de page](#prsentationdelinterface "Remonter")</sub>


## III. Vue correspondant ou Personnel de Direction : Interface standard
La vue par défaut avec le profil **Correspondant** et **Personnel de direction** est plus compléte. Vous avez en page d'accueil les tickets déjà créés, différents onglets permettant de sélectionner différents modules. Une différence à noter, le profile **Personnel de Direction**  ne dispose pas des droits pour créer des tickets mais permet de visualiser les tickets présents sur l'EPLE. Le profil ne permet pas l'ajout de matériels mais de visualiser l'ensemble du parc. La modification d'une fiche de matériel reste possible.

![](./img/page_Accueil_COR.png)

1. `Menu utilisateur` :
 - La loupe permet une recherche par mot clé
 - Le "?"" permet d’accéder à l’aide
 - "L'étoile" permet de visionner vos vues personnalisées enregistrées dans l'outil
 - "L'engrenage" ou le "Nom utilisateur" permet de choisir vos préférences personnelles (ex:ajout d'un numéro de téléphone)
 - Le "bouton" permet de se déconnecter

2. `Parc` : Affiche la vue inventaire des Ordinateurs par défaut. Plusieurs  critères de recherche, basés sur les champs de la fiche matériel, sont à définir. Tous matériels inventoriés et enregistré dans l'outil peuvent être visibles par rapport à votre filtre. En cliquant sur l'`étoile` vous avez la possibilité d'enregistrer votre recherche validée.

3. `Assistance` : Affiche la vue générale des tickets. Permet de créer, rechercher des tickets sur votre EPLE pour interagir sur des tickets "En cours", "En attente", ou "Résolu". Les tickets "Clos" ne peuvent pas être rouvert ou modifiés.

4. `Gestion` : Affiche les documents ou licences de votre EPLE enregistrés dans l'outil.

5. `Outils` Affiche la vue des projets par défaut. Vous pouvez aussi trouver la base de connaissance en utilisant le menu déroulant du module.

6. `Configuration` : Permet de gérer vos informations générales sur votre EPLE. Exemple : Ajout de lieux, IP, nom de domaine, etc...

7. `Barre des vues` : Par défaut la vue est sur "vue personnelle". Vous avez la possibilité de filtrer ces vues afin de trouver plus facilement vos tickets sur la page d'accueil.

8. `Votre planning` : Affiche des tâches programmées et non terminées selon différents contextes (ticket, projet,...). Ces tâches doivent être publiques et affectées à votre nom utilisateur ou groupe.

9. `Notes personnelles` : Ce module vous permet de vous faire des notes personnelles en cliquant sur le `+`.

10. `Notes publiques` : Affiche des informations diffusées par la Région  
<sub>[Haut de page](#prsentationdelinterface "Remonter")</sub>

## IIII. Récupérer son accès ENT MonLycee.net
Vous pouvez consulter les différentes fiches depuis la page [guide](http://monlycee.net/pages/guides "Page listant les différentes fiches guide") de l'ENT [MonLycée.net](https://ent.iledefrance.fr "Page de connexion de l'ENT").
En cas de perte de votre mot de passe ou identifiant ENT, vous avez la possibilité de récupérer vos accès en suivant les étapes ci-dessous.

![](./img/besoin_d_aide.png)

Cliquez sur `Besoin d'aide`

### a. Mot de passe perdu
Cliquez sur `Vous avez perdu votre mot de passe`, ajouter votre identifiant et `Envoyer`. Vous recevrez un courriel de confirmation de vos accès dans les minutes qui suivent.

![](./img/MDP_perdu_1.png)

![](./img/MDP_perdu_2.png)  
<sub>[Haut de page](#prsentationdelinterface "Remonter")</sub>

### b. Identifiant oublié
Cliquez sur `Vous avez perdu votre identifiant`, ajouter votre adresse mél et `Envoyé`. Vous recevrez un courriel de confirmation de vos accès dans les minutes qui suivent .

![](./img/id_perdu.png)

![](./img/id_perdu_2.png)

Si vous le message d'erreur ci-dessous apparaît, cela signifie que votre compte ENT n'est plus valide.

![](./img/user_inconnu.png)

Il vous faut aolrs une création ou une réactivation de compte ENT.  
<sub>[Haut de page](#prsentationdelinterface "Remonter")</sub>
